<?php

use Civi\Test\EndToEndInterface;
/**
 * This is a generic test class for the extension (implemented with PHPUnit).
 * @group e2e
 * @see cv
 */
class CRM_Toolbox_PaymentMethodUnitTest extends PHPUnit\Framework\TestCase implements EndToEndInterface {
  public $paymentMethodName = 'Blue Media Test Method';

  /**
   * @return void
   * @throws CRM_Core_Exception
   * @throws \Civi\API\Exception\UnauthorizedException
   */
  public function tearDown(): void {
    $this->delete();
  }

  /**
   * Check method set()
   */
  public function testSetOption() {
    $paymentMethodId = null;

    CRM_Toolbox_PaymentMethod::set(__METHOD__, $this->paymentMethodName, [
      'financial_account' => [
        'name' => $this->paymentMethodName . ' Account',
        'type' => 'Asset',
        'accounting_code' => 'BMTRX',
        'is_active' => 1,
        'is_tax' => FALSE,
        'is_deductible' => FALSE,
        'is_header_account' => FALSE,
      ]
    ]);

    $paymentMethod = [];
    try {
      $paymentMethod = \Civi\Api4\OptionValue::get(FALSE)
        ->addWhere('option_group_id:name', '=', 'payment_instrument')
        ->addWhere('label', '=', $this->paymentMethodName)
        ->execute()->single();
    } catch (API_Exception $exception) {}

    $this->assertArrayHasKey('id', $paymentMethod, 'Payment method was not created');

  }

  /**
   * @return void
   * @throws CRM_Core_Exception
   * @throws \Civi\API\Exception\UnauthorizedException
   */
  public function delete() {

    \Civi\Api4\EntityFinancialAccount::delete(FALSE)
      ->addWhere('financial_account_id:name', '=', $this->paymentMethodName . ' Account')
      ->execute();

    \Civi\Api4\OptionValue::delete(FALSE)
      ->addWhere('option_group_id:name', '=', 'payment_instrument')
      ->addWhere('label', '=', $this->paymentMethodName)
      ->execute();

    \Civi\Api4\FinancialAccount::delete(FALSE)
      ->addWhere('name', '=', $this->paymentMethodName . ' Account')
      ->execute();
  }
}
