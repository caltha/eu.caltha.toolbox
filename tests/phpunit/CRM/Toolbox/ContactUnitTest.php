<?php

use Civi\Test\EndToEndInterface;
/**
 * This is a generic test class for the extension (implemented with PHPUnit).
 * @group e2e
 * @see cv
 */
class CRM_Toolbox_ContactUnitTest extends PHPUnit\Framework\TestCase implements EndToEndInterface {
  use \Civi\Test\Api3TestTrait;
  use \Civi\Test\ContactTestTrait;

  private int $contactId;

  public function setUp(): void {
    $this->contactId = $this->individualCreate();
  }

  public function tearDown(): void {
    $this->contactDelete($this->contactId);
  }

  /**
   * Test add new subtypes contact.
   */
  public function testAddSubtypesToContactWithOneSubtype():void {

    $this->clearSubtypes($this->contactId, ['Staff']);
    CRM_Toolbox_Contact::addSubTypes($this->contactId, ['Student', 'Parent']);

    $existingSubTypes = $this->subTypesHelper($this->contactId);

    foreach (['Student', 'Parent', 'Staff'] as $item) {
      $this->assertContains($item, $existingSubTypes);
    }

  }

  /**
   * Test add new subtypes contact.
   */
  public function testAddSubtypesToContactWithMultipleSubtype():void {

    $this->clearSubtypes($this->contactId, ['Student', 'Staff']);
    CRM_Toolbox_Contact::addSubTypes($this->contactId, ['Student', 'Parent']);

    $existingSubTypes = $this->subTypesHelper($this->contactId);

    foreach (['Student', 'Parent', 'Staff'] as $item) {
      $this->assertContains($item, $existingSubTypes);
    }

  }
  /**
   * Test remove one subtype from list of contacts.
   */
  public function testRemoveSubtypeFromContactWithOneSubtype():void {

    $removed = ['Student'];

    $this->clearSubtypes($this->contactId, ['Student']);
    CRM_Toolbox_Contact::removeSubTypes($this->contactId, ['Student', 'Parent']);

    $existingSubTypes = $this->subTypesHelper($this->contactId);

    foreach ($removed as $item) {
      $this->assertNotContains($item, $existingSubTypes);
    }

  }

  /**
   * Test remove subtypes from list of contacts.
   */
  public function testRemoveSubtypesFromContactWithMultipleSubtypes():void {

    $removed = ['Student', 'Parent'];

    $this->clearSubtypes($this->contactId, ['Student', 'Parent', 'Staff']);
    CRM_Toolbox_Contact::removeSubTypes($this->contactId, $removed);

    $existingSubTypes = $this->subTypesHelper($this->contactId);

    foreach ($removed as $item) {
      $this->assertNotContains($item, $existingSubTypes);
    }
    foreach (['Staff'] as $item) {
      $this->assertContains($item, $existingSubTypes);
    }
  }


  /**
   * Helper return list of contact subtypes
   * @param int $contactId
   */
  public function subTypesHelper( int $contactId)
  {
    $existingSubTypes = [];
    try {
      $result = \Civi\Api4\Contact::get(FALSE)
        ->addSelect('contact_sub_type')
        ->addWhere('id', '=', $contactId)
        ->execute()->single();

      $existingSubTypes = array_values((array) ($result['contact_sub_type'] ?? []));

      sort($existingSubTypes);

    } catch (API_Exception $exception) {}

    return $existingSubTypes;
  }

  /**
   * Clear/update subtypes of contact.
   */
  public function clearSubtypes($contactId, $contactSubTypes):void {
    \Civi\Api4\Contact::update(FALSE)
      ->addValue('contact_sub_type', $contactSubTypes)
      ->addWhere('id', '=', $contactId)
      ->execute();
  }

}
