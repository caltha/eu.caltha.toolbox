<?php

trait CRM_Toolbox_Tag {

  /**
   * Set tag in cache and return id
   *
   * @param string $key
   * @param string $name
   * @param array $options [ color | description ]
   *
   * @return int
   * @throws \CiviCRM_API3_Exception
   */
  public static function set(string $key, string $name, array $options = []): int {
    $cache = Civi::cache()->get($key);
    if (!isset($cache)) {
      $id = self::create($name, $options);
      Civi::cache()->set($key, $id);
      return $id;
    }

    return $cache;
  }

  /**
   * Create new tag for contact. You can override used_for in options.
   * Use $options['color'] = '#000000' to set color.
   *
   * @param string $name
   * @param array $options
   *
   * @return int
   * @throws \CiviCRM_API3_Exception
   */
  private static function create(string $name, array $options = []): int {
    $params = [
      'sequential' => 1,
      'name' => $name,
    ];
    $result = civicrm_api3('Tag', 'get', $params);
    if ($result['count'] == 0) {
      $params['name'] = $name;
      $params['is_selectable'] = 1;
      $params['is_reserved'] = 0;
      $params['is_tagset'] = 0;
      $params['used_for'] = 'civicrm_contact';
      $params = array_merge($params, $options);
      $result = civicrm_api3('Tag', 'create', $params);
    }

    return (int) $result['values'][0]['id'];
  }

  /**
   * Dodawanie tagu do kontaktu
   *
   * @param $tagId
   * @param $contactId
   * @return \Civi\Api4\Generic\Result
   * @throws CRM_Core_Exception
   * @throws \Civi\API\Exception\UnauthorizedException
   */
  public static function addTagToContact($tagId, $contactId) {
    $tag = \Civi\Api4\EntityTag::get(FALSE)
      ->selectRowCount()
      ->addWhere('entity_table', '=', 'civicrm_contact')
      ->addWhere('entity_id', '=', $contactId)
      ->addWhere('tag_id', '=', $tagId)
      ->execute();
    if($tag->rowCount == 0) {
      $tag = \Civi\Api4\EntityTag::create(FALSE)
        ->addValue('entity_table', 'civicrm_contact')
        ->addValue('entity_id', $contactId)
        ->addValue('tag_id', $tagId)
        ->execute();
    }
    return $tag;
  }
}
