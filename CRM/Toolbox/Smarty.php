<?php

class CRM_Toolbox_Smarty {

  /**
   * Compile string based on template.
   *
   * @param string $template
   *
   * @return string
   */
  public static function compile($template) {
    $smarty = CRM_Core_Smarty::singleton();
    $smarty->assign('date', date('YmdHis'));
    return $smarty->fetch('string:' . $template);
  }

}
