<?php

trait CRM_Toolbox_PaymentMethod {

  /**
   * Set payment method in cache and return id
   *
   * @param string $key
   * @param string $name
   *
   * @return int|mixed
   * @throws \CiviCRM_API3_Exception
   */
  public static function set(string $key, string $name, array $options = []): int {
    $cache = Civi::cache('long')->get($key);
    if (!isset($cache)) {
      $id = self::setOption($name, $options);
      Civi::cache('long')->set($key, $id);
      return $id;
    }

    return $cache;
  }

  /**
   * @param string $name
   * @param array $options
   * @return int
   * @throws API_Exception
   * @throws \Civi\API\Exception\UnauthorizedException
   *
   * Financial account type must be pass as name like:
   * "Asset","Liability","Revenue","Cost of Sales","Expenses",
   */
  private static function setOption(string $name, array $options = []): int {
    if(isset($options['financial_account'])) {
      $params = array_merge( $options['financial_account'], [
        'financial_account_type_id' => CRM_Core_PseudoConstant::getKey('CRM_Financial_DAO_FinancialAccount', 'financial_account_type_id', $options['financial_account']['type']),
      ]);
      $defaults = [];
      $financialAccount = CRM_Core_DAO::commonRetrieve('CRM_Financial_BAO_FinancialAccount', $params, $defaults);

      if(!$financialAccount) {
        $financialAccount = CRM_Financial_BAO_FinancialAccount::writeRecord($params);
      }
      $options['financial_account_id'] = $financialAccount->id;
    }

    return (int) CRM_Toolbox_Options::setValue('payment_instrument', $name, $options);
  }

}
