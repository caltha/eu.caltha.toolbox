<?php

trait CRM_Toolbox_RelationshipType {

  /**
   * Set relationship type in cache and return id.
   *
   * Required options:
   * - contact_type_a [ Individual | Organization | Household ]
   * - contact_type_b [ Individual | Organization | Household ]
   *
   * Supported options:
   * - contact_sub_type_a
   * - contact_sub_type_b
   * - is_active [ 0 | 1 (default) ]
   *
   * @param string $key
   * @param string $nameAB
   * @param string $nameBA
   * @param array $options required options: contact_type_a, contact_type_b
   * @return int
   * @throws API_Exception
   * @throws \Civi\API\Exception\UnauthorizedException
   */
  public static function set(string $key, string $nameAB, string $nameBA, array $options): int {
    $cache = Civi::cache('long')->get($key);
    if (!isset($cache)) {
      $id = self::setType($nameAB, $nameBA, $options);
      Civi::cache('long')->set($key, $id);
      return $id;
    }

    return $cache;
  }

  /**
   * @param string $nameAB
   * @param string $nameBA
   * @param array $options
   * @return mixed
   * @throws API_Exception
   * @throws \Civi\API\Exception\UnauthorizedException
   */
  private static function setType(string $nameAB, string $nameBA, array $options): int {
    try {
      $relationshipType = \Civi\Api4\RelationshipType::get(false)
        ->addSelect('id')
        ->addWhere('name_a_b', '=', $nameAB)
        ->addWhere('name_b_a', '=', $nameBA)
        ->execute()
        ->single();
    } catch (API_Exception $exception) {
      self::checkFields(['contact_type_a', 'contact_type_b'], $options);
      $options = array_replace(['label_a_b' => $nameAB, 'label_b_a' => $nameBA], $options);
      $relationshipTypeCreate = \Civi\Api4\RelationshipType::create(false)
        ->addValue('name_a_b', $nameAB)
        ->addValue('name_b_a', $nameBA);
      foreach ($options as $key => $value) {
        $relationshipTypeCreate->addValue($key, $value);
      }
      $relationshipType = $relationshipTypeCreate->execute()->single();
    }

    return $relationshipType['id'];
  }

  /**
   * @param array $requiredFields
   * @param array $params
   * @return bool
   * @throws API_Exception
   */
  private static function checkFields(array $requiredFields, array $params): bool {
    $missingFields = [];
    foreach ($requiredFields as $field) {
      if (!array_key_exists($field, $params) || !$params[$field]) {
        $missingFields[] = $field;
      }
    }
    if ($missingFields) {
      $message = sprintf('Missing required fields: %s', implode(', ', $missingFields));
      throw new API_Exception($message);
    }

    return TRUE;
  }

}
