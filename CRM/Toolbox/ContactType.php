<?php

trait CRM_Toolbox_ContactType {

  /**
   * Set subtype for Household.
   *
   * @param $name
   * @param $label
   *
   * @return mixed
   * @throws \CiviCRM_API3_Exception
   */
  public static function household($name, $label) {
    return self::setType($name, $label, 'Household');
  }

  /**
   * Set subtype for Individual.
   *
   * @param $name
   * @param $label
   *
   * @return mixed
   * @throws \CiviCRM_API3_Exception
   */
  public static function individual($name, $label) {
    return self::setType($name, $label, 'Individual');
  }

  /**
   * Set subtype for Organization.
   *
   * @param $name
   * @param $label
   *
   * @return mixed
   * @throws \CiviCRM_API3_Exception
   */
  public static function organization($name, $label) {
    return self::setType($name, $label, 'Organization');
  }

  /**
   * @param string $name
   * @param string $label
   * @param string $parent
   *
   * @return mixed
   * @throws \CiviCRM_API3_Exception
   */
  private static function setType($name, $label, $parent) {
    $params = [
      'sequential' => 1,
      'name' => $name,
    ];
    $result = civicrm_api3('ContactType', 'get', $params);
    if ($result['count'] == 0) {
      $params = [
        'sequential' => 1,
        'name' => $name,
        'label' => $label,
        'parent_id' => $parent,
        'is_active' => 1,
        'is_reserved' => 0,
      ];
      $result = civicrm_api3('ContactType', 'create', $params);
    }

    return $result['id'];
  }

}