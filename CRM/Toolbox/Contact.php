<?php

trait CRM_Toolbox_Contact {

  /**
   * Add new subtypes to list of contacts.
   *
   * @param array $contactIdsSubtypes Array of names from civicrm_contact_type.name
   * with array key as contactId column. Example:
   * [1 => ['WOL', 'SYM'], 2 => ['WOL'], 3 => ['POD', 'PDOBE', 'DRZSA']]
   *
   * @return void
   * @throws \CiviCRM_API3_Exception
   */
  public static function addSubtypesToContacts(array $contactIdsSubtypes): void {
    foreach ($contactIdsSubtypes as $contactId => $newSubTypes) {
      self::addSubTypes($contactId, $newSubTypes);
    }
  }

  /**
   * Remove subtypes from list of contacts.
   *
   * @param array $contactIdsSubtypes Array of names from civicrm_contact_type.name
   * with array key as contactId column. Example:
   * [1 => ['WOL', 'SYM'], 2 => ['WOL'], 3 => ['POD', 'PDOBE', 'DRZSA']]
   *
   * @return void
   * @throws \CiviCRM_API3_Exception
   */
  public static function removeSubtypesFromContacts(array $contactIdsSubtypes): void {
    foreach ($contactIdsSubtypes as $contactId => $removedSubTypes) {
      self::removeSubTypes($contactId, $removedSubTypes);
    }
  }

  /**
   * Add new contact subtypes.
   *
   * @param int $contactId
   * @param array $newSubTypes Array of names from civicrm_contact_type.name
   *   column.
   *
   * @return bool
   * @throws \CiviCRM_API3_Exception
   */
  public static function addSubTypes($contactId, $newSubTypes) {
    try {
      $result = \Civi\Api4\Contact::get(FALSE)
        ->addSelect('contact_sub_type')
        ->addWhere('id', '=', $contactId)
        ->execute()->single();

      if ($contactId) {
        $existingSubTypes = (array) ($result['contact_sub_type'] ?? []);
        $missingSubTypes = array_diff($newSubTypes, $existingSubTypes);
        if ($missingSubTypes) {
          $contactSubTypes = array_merge($existingSubTypes, $newSubTypes);
          \Civi\Api4\Contact::update(FALSE)
            ->addValue('contact_sub_type', $contactSubTypes)
            ->addWhere('id', '=', $contactId)
            ->execute();
        }
      }
    } catch (API_Exception $exception) {
     return FALSE;
    }
    return TRUE;
  }

  /**
   * Remove given subtypes from contact.
   *
   * @param int $contactId
   * @param array $removedSubTypes Array of names from civicrm_contact_type.name
   *   column.
   *
   * @return bool
   * @throws \CiviCRM_API3_Exception
   */
  public static function removeSubTypes($contactId, $removedSubTypes) {
    try {
      $result = \Civi\Api4\Contact::get(FALSE)
        ->addSelect('contact_sub_type')
        ->addWhere('id', '=', $contactId)
        ->execute()->single();

      if ($contactId) {
        $existingSubTypes = (array) ($result['contact_sub_type'] ?? []);
        $newSubTypes = array_diff($existingSubTypes, $removedSubTypes);

        \Civi\Api4\Contact::update(FALSE)
          ->addValue('contact_sub_type', $newSubTypes)
          ->addWhere('id', '=', $contactId)
          ->execute();
      }
    } catch (API_Exception $exception) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * @param string $contactSubType in db format, civicrm_contact.contact_sub_type
   *
   * @return array
   */
  public static function extractSubTypes($contactSubType) {
    $t = explode(CRM_Core_DAO::VALUE_SEPARATOR, $contactSubType);
    return array_filter($t);
  }

  /**
   * @param $contactId
   *
   * @return array
   */
  private static function getContactSubTypes($contactId) {
    $contact = new CRM_Contact_BAO_Contact();
    $contact->id = $contactId;
    $contact->find(TRUE);

    return self::extractSubTypes($contact->contact_sub_type);
  }

  /**
   * @param array $currentSubTypes
   * @param array $newSubTypes
   * @param array $reference
   *
   * @return array
   */
  private static function findAddedSubTypes($currentSubTypes, $newSubTypes, $reference) {
    $diff = [];
    if(is_array($newSubTypes) && is_array($currentSubTypes)) {
      $diff = array_diff($newSubTypes, $currentSubTypes);
      foreach ($diff as $key => $item) {
        if (!in_array($item, $reference)) {
          unset($diff[$key]);
        }
      }
    }
    return $diff;
  }

  /**
   * @param array $currentSubTypes
   * @param array $newSubTypes
   * @param array $reference
   *
   * @return array
   */
  private static function findRemovedSubTypes($currentSubTypes, $newSubTypes, $reference) {
    $diff = [];
    if(is_array($currentSubTypes) && is_array($newSubTypes)) {
      $diff = array_diff($currentSubTypes, $newSubTypes);
      foreach ($diff as $key => $item) {
        if (!in_array($item, $reference)) {
          unset($diff[$key]);
        }
      }
    }
    return $diff;
  }

  /**
   * Url to contact view page
   *
   * @param int $id contact id
   * @return string
   */
  public static function url(int $id): string {
    return CRM_Utils_System::url(
      'civicrm/contact/view',
      [
        'reset' => 1,
        'cid' => $id,
      ],
      TRUE
    );
  }

  /**
   * Return id of logged contact, otherwise returns admin contact id.
   * Useful for hooks for anonymous contacts.
   *
   * @return int|null
   */
  public static function createdId() {
    if (!CRM_Core_Session::getLoggedInContactID()) {
      return 2;
    }

    return CRM_Core_Session::getLoggedInContactID();
  }

  /**
   * Return id of contact, based on activity ID
   *
   * @return int|null
   */
  public static function contactIdByActivity($activityId, $sourceId = 3) {
    return \Civi\Api4\Activity::get(FALSE)
      ->addSelect('activity_contact.contact_id')
      ->addJoin('ActivityContact AS activity_contact', 'LEFT', ['activity_contact.activity_id', '=', 'id'], ['activity_contact.record_type_id', '=', $sourceId])
      ->addWhere('id', '=', $activityId)
      ->execute()
      ->single()['activity_contact.contact_id'];
  }

}
