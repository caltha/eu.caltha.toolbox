<?php

trait CRM_Toolbox_Group {

  /**
   * Set group in cache and return id
   *
   * @param string $key
   * @param string $name
   * @param array $options
   *
   * @return int|mixed
   * @throws \CiviCRM_API3_Exception
   */
  public static function set($key, $name, $options = []) {
    $cache = Civi::cache()->get($key);
    if (!isset($cache)) {
      $id = self::create($name, $options);
      Civi::cache()->set($key, $id);
      return $id;
    }

    return $cache;
  }

  /**
   * Create new group.
   *
   * @param string $name
   * @param array $options
   *
   * @return int
   * @throws \CiviCRM_API3_Exception
   */
  private static function create($name, $options = []): int {
    $params = [
      'sequential' => 1,
      'name' => $name,
    ];
    $result = civicrm_api3('Group', 'get', $params);
    if ($result['count'] == 0) {
      $params['title'] = $name;
      $params['is_active'] = 1;
      $params['group_type'] = 'Mailing List';
      $params = array_merge($params, $options);
      $result = civicrm_api3('Group', 'create', $params);
    }

    return (int) $result['values'][0]['id'];
  }

}
