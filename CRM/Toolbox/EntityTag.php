<?php

trait CRM_Toolbox_EntityTag {

  /**
   * Set tag to contact.
   *
   * @param int $contactId
   * @param int $tagId
   *
   * @return bool
   * @throws \CiviCRM_API3_Exception
   */
  public static function create($contactId, $tagId) {
    if ($contactId && $tagId) {
      $params = [
        'sequential' => 1,
        'entity_table' => "civicrm_contact",
        'entity_id' => $contactId,
        'tag_id' => $tagId,
      ];
      $result = civicrm_api3('EntityTag', 'get', $params);
      if ($result['count'] == 0) {
        $result = civicrm_api3('EntityTag', 'create', $params);
        return (int) $result['total_count'];
      }
    }

    return 0;
  }

  /**
   * Remove tag from contact.
   *
   * @param int $contactId
   * @param int $tagId
   *
   * @return bool
   * @throws \CiviCRM_API3_Exception
   */
  public static function remove($contactId, $tagId) {
    if ($contactId && $tagId) {
      $params = [
        'sequential' => 1,
        'entity_table' => "civicrm_contact",
        'entity_id' => $contactId,
        'tag_id' => $tagId,
      ];
      $result = civicrm_api3('EntityTag', 'getsingle', $params);
      if (CRM_Utils_Array::value('id', $result)) {
        $paramsDelete = [
          'sequential' => 1,
          'entity_table' => "civicrm_contact",
          'entity_id' => $contactId,
          'tag_id' => $tagId,
        ];
        $result = civicrm_api3('EntityTag', 'delete', $paramsDelete);
        return (int) $result['removed'];
      }
    }

    return 0;
  }

  /**
   * Set given tag for contacts.
   *
   * @param array $contactIds
   * @param int $tagId
   *
   * @return array
   * @throws \CiviCRM_API3_Exception
   */
  public function setInBatch(array $contactIds, int $tagId): array {
    $values = [];
    foreach ($contactIds as $contactId) {
      $result = self::create($contactId, $tagId);
      $values[$contactId] = $result;
    }

    return $values;
  }

  /**
   * Remove given tag from contacts.
   *
   * @param array $contactIds
   * @param int $tagId
   *
   * @return array
   * @throws \CiviCRM_API3_Exception
   */
  public function removeInBatch(array $contactIds, int $tagId): array {
    $values = [];
    foreach ($contactIds as $contactId) {
      $result = self::remove($contactId, $tagId);
      $values[$contactId] = $result;
    }

    return $values;
  }

}
