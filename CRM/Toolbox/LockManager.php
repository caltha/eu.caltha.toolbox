<?php

class CRM_Toolbox_LockManager {

  private $name;
  private $lock;

  /**
   * Assumption: lockname is generated based on filename of current file.
   * So on one file should be only one lockManager.
   * Other assumption: each api action has own file.
   *
   * And this is a worker lock.
   *
   * @param string $path set __FILE__
   * @param string $extensionLongName set E::LONG_NAME
   *
   * @throws \CRM_Core_Exception
   */
  public function __construct(string $path, string $extensionLongName) {
    $filename = pathinfo($path, PATHINFO_FILENAME);
    $this->name = sprintf('worker.%s.%s', $extensionLongName, $filename);
    $this->lock = Civi::lockManager()->acquire($this->name);
  }

  /**
   * @return bool
   */
  public function isRunning(): bool {
    return !$this->lock->isAcquired();
  }

  /**
   * @return array
   */
  public function createError(): array {
    $msg = sprintf('Could not acquire lock, another %s process is running', $this->name);
    return civicrm_api3_create_error($msg);
  }

  public function release() {
    $this->lock->release();
  }

}
