<?php

class CRM_Toolbox_Session {

  /**
   * Non-expiring info status
   * @param string $title
   * @param string $text
   */
  public static function info(string $title, string $text) {
    CRM_Core_Session::setStatus($text, $title, 'info', ['expires' => 0]);
  }

  /**
   * Non-expiring success status
   * @param string $title
   * @param string $text
   */
  public static function success(string $title, string $text) {
    CRM_Core_Session::setStatus($text, $title, 'success', ['expires' => 0]);
  }

  /**
   * Non-expiring error status
   * @param string $title
   * @param string $text
   */
  public static function error(string $title, string $text) {
    CRM_Core_Session::setStatus($text, $title, 'error', ['expires' => 0]);
  }

}
