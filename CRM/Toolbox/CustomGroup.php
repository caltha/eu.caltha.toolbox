<?php

trait CRM_Toolbox_CustomGroup {

  /**
   * @param string $groupName
   *
   * @return int
   * @throws CiviCRM_API3_Exception
   */
  protected static function getCustomGroup(string $groupName): int {
    $result = civicrm_api3('CustomGroup', 'get', [
      'sequential' => 1,
      'name' => $groupName,
    ]);

    return (int) $result['id'];
  }

  /**
   * @param string $groupName
   *
   * @return string
   * @throws CiviCRM_API3_Exception
   */
  protected static function getCustomGroupTitle(string $groupName): string {
    $result = civicrm_api3('CustomGroup', 'getsingle', [
      'sequential' => 1,
      'name' => $groupName,
      'return' => ['title'],
    ]);

    return $result['title'];
  }

  /**
   * Supported options:
   * - title
   * - extends
   * - extends_entity_column_value
   * - table_name
   * - style [Inline | ? ]
   * - collapse_adv_display [ 0 | 1 ]
   * - collapse_display [ 0 | 1 ]
   * - is_public [ 0 | 1 ]
   * - is_reserved [ 0 | 1 ]
   *
   * @param string $key
   * @param string $groupName
   * @param array $options
   * @return int
   * @throws CiviCRM_API3_Exception
   */
  protected static function set(string $key, string $groupName, array $options = []): int {
    $cache = Civi::cache('long')->get($key);
    if (!isset($cache)) {
      $id = self::setGroup($groupName, $options);
      Civi::cache('long')->set($key, $id);
      return $id;
    }

    return $cache;
  }

  /**
   * @param string $groupName
   * @param array $options keys: title, extends, extends_entity_column_value, table_name
   * @return int
   * @throws CiviCRM_API3_Exception
   */
  private static function setGroup(string $groupName, array $options = []): int {
    $result = civicrm_api3('CustomGroup', 'get', [
      'sequential' => 1,
      'name' => $groupName,
    ]);
    if ($result['count'] == 0) {
      $params = [
        'sequential' => 1,
        'name' => $groupName,
      ] + $options;
      $result = civicrm_api3('CustomGroup', 'create', $params);
    }

    return (int) $result['id'];
  }

  /**
   * Get custom group id for given name group.
   *
   * @param string $key
   * @param string $groupName
   *
   * @return int
   * @throws CiviCRM_API3_Exception
   */
  public static function get(string $key, string $groupName): int {
    $cache = Civi::cache('long')->get($key);
    if (!isset($cache)) {
      $id = self::getCustomGroup($groupName);
      Civi::cache('long')->set($key, $id);
      return $id;
    }

    return $cache;
  }

  /**
   * Get custom group id for given name group.
   *
   * @param string $key
   * @param string $groupName
   *
   * @return string
   * @throws \CiviCRM_API3_Exception
   */
  public static function getTitle(string $key, string $groupName) {
    $cache = Civi::cache('long')->get($key);
    if (!isset($cache)) {
      $id = self::getCustomGroupTitle($groupName);
      Civi::cache('long')->set($key, $id);
      return $id;
    }

    return $cache;
  }

  /**
   * Allow to erase custom group with fields, ex: while testing
   *
   * @param $groupName
   * @return true
   */
  public static function eraseCustomGroup($groupName) {
    try {
      $result = \Civi\Api4\CustomGroup::get(FALSE)
        ->addWhere('name', '=', $groupName)
        ->execute()
        ->single();

      if ($result) {
        \Civi\Api4\CustomGroup::delete(FALSE)
          ->addWhere('id', '=', $result['id'])
          ->execute();
      }

    } catch (API_Exception $exception) {}

    return TRUE;
  }

}
