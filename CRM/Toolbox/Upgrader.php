<?php
use CRM_Toolbox_ExtensionUtil as E;

/**
 * Collection of upgrade steps.
 */
class CRM_Toolbox_Upgrader extends CRM_Extension_Upgrader_Base {

  public function postInstall() {
    $sqlQuery = file_get_contents(__DIR__ . '/../../sql/group-contact.sql', TRUE);
    $this->executeCustomSql($sqlQuery);
  }

  /**
   * @param $sqlQuery
   */
  private function executeCustomSql($sqlQuery) {
    $string = CRM_Utils_File::stripComments($sqlQuery);
    $queries = preg_split('/@@@$/m', $string);
    foreach ($queries as $query) {
      $query = trim($query);
      if (!empty($query)) {
        CRM_Core_DAO::executeQuery($query);
      }
    }
  }

}
