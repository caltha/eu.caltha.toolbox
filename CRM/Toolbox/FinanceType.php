<?php

/**
 * @deprecated use CRM_Toolbox_FinancialType
 */
trait CRM_Toolbox_FinanceType {

  /**
   * Set financial type in cache and return id
   *
   * @param string $key
   * @param string $name
   *
   * @return int
   * @throws CiviCRM_API3_Exception
   */
  public static function set(string $key, string $name): int {
    $cache = Civi::cache('long')->get($key);
    if (!isset($cache)) {
      $id = self::setType($name);
      Civi::cache('long')->set($key, $id);
      return $id;
    }

    return $cache;
  }

  /**
   * @param string $name
   *
   * @return mixed
   * @throws CiviCRM_API3_Exception
   */
  private static function setType(string $name): int {
    $params = [
      'sequential' => 1,
      'name' => $name,
    ];
    $result = civicrm_api3('FinancialType', 'get', $params);
    if ($result['count'] == 0) {
      $params['is_active'] = 1;
      $result = civicrm_api3('FinancialType', 'create', $params);
    }

    return (int) $result['id'];
  }

}
