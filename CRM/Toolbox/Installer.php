<?php

trait CRM_Toolbox_Installer {

  /**
   * Instaluje wszystkie typy z klasy np.
   * CRM_Toolbox_Installer::installMethods('CRM_Dontus_Model_FinancialType');
   *
   * @param string $className
   *
   * @return void
   * @throws \ReflectionException
   */
  public static function installMethods(string $className){
    $f = new ReflectionClass($className);
    $methodsNotAlowed = ['create', 'set', 'setOption', 'setType'];
    foreach ($f->getMethods() as $m) {
      if ($m->class == $className && !in_array($m->name, $methodsNotAlowed)) {
        forward_static_call_array([$className, $m->name], []);
      }
    }
  }
}