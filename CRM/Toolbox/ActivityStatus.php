<?php

trait CRM_Toolbox_ActivityStatus {

  /**
   * Set activity status in cache and return id
   *
   * Supported options:
   * - label: in order to translated text via E::ts() method
   * - filter: [ 0 incomplete | 1 completed | 2 cancelled ]
   * - color: in HEX format like #FF0000
   *
   * @param string $key
   * @param string $name
   * @param array $options
   * @return int
   * @throws CiviCRM_API3_Exception
   */
  public static function set(string $key, string $name, array $options = []): int {
    $cache = Civi::cache('long')->get($key);
    if (!isset($cache)) {
      $id = self::setOption($name, $options);
      Civi::cache('long')->set($key, $id);
      return $id;
    }

    return $cache;
  }

  /**
   * @param string $name
   * @param array $options
   * @return int
   * @throws API_Exception
   * @throws \Civi\API\Exception\UnauthorizedException
   */
  private static function setOption(string $name, array $options = []): int {
    return (int) CRM_Toolbox_Options::setValue('activity_status', $name, $options);
  }

}
