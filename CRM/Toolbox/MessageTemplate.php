<?php

use Civi\API\Exception\UnauthorizedException;

trait CRM_Toolbox_MessageTemplate {

  /**
   * Get id of template given by workflow name.
   *
   * Be careful - there could be more than 1 message template with the same workflow name.
   *
   * @param string $workflowName
   * @return int
   * @throws API_Exception
   * @throws UnauthorizedException
   */
  public static function getId(string $workflowName): int {
    $msg = \Civi\Api4\MessageTemplate::get(FALSE)
      ->addWhere('workflow_name', '=', $workflowName)
      ->setSelect(['id'])
      ->execute()
      ->first();

    return CRM_Utils_Array::value('id', $msg, 0);
  }

  /**
   * @param array $params
   * @return int
   * @throws API_Exception
   * @throws UnauthorizedException
   */
  public static function save(array $params): int {
    $msg = \Civi\Api4\MessageTemplate::save(FALSE)->setRecords([$params])->execute()->first();

    return $msg['id'];
  }

}
