<?php

trait CRM_Toolbox_RecordType {

  /**
   * @return int
   */
  public static function sourceId(): int {
    return (int) CRM_Core_PseudoConstant::getKey('CRM_Activity_BAO_ActivityContact', 'record_type_id', 'Activity Source');
  }

  /**
   * @return int
   */
  public static function assigneeId(): int {
    return (int) CRM_Core_PseudoConstant::getKey('CRM_Activity_BAO_ActivityContact', 'record_type_id', 'Activity Assignees');
  }

  /**
   * @return int
   */
  public static function targetId(): int {
    return (int) CRM_Core_PseudoConstant::getKey('CRM_Activity_BAO_ActivityContact', 'record_type_id', 'Activity Targets');
  }

}
