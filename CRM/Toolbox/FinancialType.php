<?php

trait CRM_Toolbox_FinancialType {

  /**
   * Set financial type in cache and return id
   *
   * @param string $key
   * @param string $name
   * @param array $options
   * @return int
   * @throws CiviCRM_API3_Exception
   */
  public static function set(string $key, string $name, array $options = []): int {
    $cache = Civi::cache('long')->get($key);
    if (!isset($cache)) {
      $id = self::setType($name, $options);
      Civi::cache('long')->set($key, $id);
      return $id;
    }

    return $cache;
  }

  /**
   * @param string $name
   * @param array $options
   * @return mixed
   * @throws CiviCRM_API3_Exception
   */
  private static function setType(string $name, array $options = []): int {
    $params = [
      'sequential' => 1,
      'name' => $name,
    ];
    $result = civicrm_api3('FinancialType', 'get', $params);
    if ($result['count'] == 0) {
      $params['is_active'] = 1;
      $params = array_merge($params, $options);
      $result = civicrm_api3('FinancialType', 'create', $params);
    }

    return (int) $result['id'];
  }

}
