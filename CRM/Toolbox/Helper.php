<?php

class CRM_Toolbox_Helper {

  /**
   * Convert %1%3% value to array [1, 3]
   * @param string $value contains values separate by CRM_Core_DAO::VALUE_SEPARATOR
   * @return array
   */
  public static function explodeSeparatedValue(string $value = ''): array {
    $value = trim($value, CRM_Core_DAO::VALUE_SEPARATOR);
    if ($value) {
      return explode(CRM_Core_DAO::VALUE_SEPARATOR, $value);
    }

    return [];
  }

  /**
   * Convert array [1, 3] to separated value used in database %1%3%
   * @param array $array
   * @return string
   */
  public static function implodeAsSeparated(array $array): string {
    return CRM_Core_DAO::VALUE_SEPARATOR . implode(CRM_Core_DAO::VALUE_SEPARATOR, $array) . CRM_Core_DAO::VALUE_SEPARATOR;
  }

  /**
   * Rename custom fields group
   *
   * @param $renameFrom
   * @param $renameTo
   * @param bool $renameTable
   * @return mixed
   */
  public static function renameGroup($renameFrom, $renameTo, bool $renameTable = FALSE) {
    try{
      $group = \Civi\Api4\CustomGroup::get(FALSE)
        ->addWhere('name', '=', $renameFrom)
        ->execute()
        ->single();

      CRM_Core_DAO::executeQuery("UPDATE civicrm_custom_group SET name = %1 WHERE id = %2;;", [
        1 => [$renameTo, 'String'],
        2 => [$group['id'], 'Integer']
      ]);

      $rename = \Civi\Api4\CustomGroup::update(FALSE)
        ->addValue('name', $renameTo);

      if($renameTable) {
        CRM_Core_DAO::executeQuery("RENAME TABLE %1 TO %2;", [
          1 => [$group['table_name'], 'Text'],
          2 => [$renameTable, 'Text']
        ]);
        $rename = $rename->addValue('table_name', $renameTable);
      }
      $rename->setReload(TRUE)
        ->addWhere('id', '=', $group['id'])
        ->execute();

      return TRUE;
    } catch(\Civi\Core\Exception\DBQueryException $e) {
      return $e->getUserInfo();
    } catch(\Exception$e) {
      return $e->getMessage();
    }
  }

  /**
   * Rename custom field name, not label
   *
   * @param $renameFrom
   * @param $renameTo
   * @return mixed
   */
  public static function renameField($renameFrom, $renameTo) {
    [$groupName, $filedName] = explode('.', $renameFrom);

    try{
      $customField = \Civi\Api4\CustomField::get(FALSE)
        ->addSelect('*', 'custom_group_id.*')
        ->addWhere('custom_group_id:name', '=', $groupName)
        ->addWhere('name', '=', $filedName)
        ->execute()
        ->single();

      [$table, $createQuery] = CRM_Core_DAO::executeQuery("SHOW CREATE TABLE %1", [
        1 => [$customField['custom_group_id.table_name'], 'Text']
      ])->getDatabaseResult()->fetchRow();

      $re = sprintf('@\`%s\`\s([^,]+)@', $customField['column_name']);
      $matches = [];
      preg_match($re, $createQuery, $matches);
      if(empty($matches) || empty($matches[1])) {
        throw new InvalidArgumentException(sprintf(
          'Brak dopasowania dla kolumny %s w zapytaniu SHOW CREATE TABLE',
          $customField['column_name']
        ));
      }

      \Civi\Api4\CustomField::update(FALSE)
        ->addValue('name', $renameTo)
        ->addValue('column_name', $renameTo)
        ->addWhere('id', '=', $customField['id'])
        ->execute();

      CRM_Core_DAO::executeQuery("ALTER TABLE %1 CHANGE %2 %3 %4;", [
        1 => [$customField['custom_group_id.table_name'], 'Text'],
        2 => [$customField['column_name'], 'Text'],
        3 => [strtolower($renameTo), 'Text'],
        4 => [$matches[1], 'Text']
      ]);

      return TRUE;
    } catch(\Civi\Core\Exception\DBQueryException $e) {
      return $e->getUserInfo();
    } catch(\Exception$e) {
      return $e->getMessage();
    }
  }

}
