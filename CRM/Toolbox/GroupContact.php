<?php

use Civi\API\Exception\UnauthorizedException;

trait CRM_Toolbox_GroupContact {

  /**
   * Add list of contact to contacts group.
   *
   * @param array $contactIdsSubtypes Array of group ids
   * with array key as contactId column. Example:
   * [1 => 12, 2 => 33]
   *
   * @return void
   * @throws \CiviCRM_API3_Exception
   */
  public static function addContactsToGroup(array $contactIdsGroups): void {
    foreach ($contactIdsGroups as $contactId => $groupId) {
      self::addContactToGroup($contactId, $groupId);
    }
  }

  /**
   * Remove list of contacts from group.
   *
   * @param array $contactIdsGroups Array of group ids with array key as contactId column. Example: [1 => 12, 2 => 33]
   * @return void
   * @throws CRM_Core_Exception
   * @throws UnauthorizedException
   */
  public static function removeContactsFromGroup(array $contactIdsGroups): void {
    foreach ($contactIdsGroups as $contactId => $groupId) {
      self::removeContactFromGroup($contactId, $groupId);
    }
  }

  /**
   * If record in civicrm_group_contact exists use update() method
   * elsewhere use save() method.
   * @param int $contactId
   * @param int $groupId
   * @throws API_Exception
   * @throws UnauthorizedException
   */
  public static function addContactToGroup(int $contactId, int $groupId) {
    $existingRecord = \Civi\Api4\GroupContact::get(FALSE)
      ->addWhere('contact_id', '=', $contactId)
      ->addWhere('group_id', '=', $groupId)
      ->execute();
    if ($existingRecord->count()) {
      $recordId = $existingRecord->first()['id'];
      $status = $existingRecord->first()['status'];
      if ($status != 'Added') {
        $results = \Civi\Api4\GroupContact::update(FALSE)
          ->addWhere('id', '=', $recordId)
          ->addValue('status', 'Added')
          ->execute();
      }
    }
    else {
      $results = \Civi\Api4\GroupContact::save(FALSE)
        ->addRecord(['contact_id' => $contactId, 'group_id' => $groupId])
        ->execute();
    }
  }

  /**
   * @param int $contactId
   * @param int $groupId
   * @return void
   * @throws CRM_Core_Exception
   * @throws UnauthorizedException
   */
  public static function removeContactFromGroup(int $contactId, int $groupId):void {
    $existingRecord = \Civi\Api4\GroupContact::get(FALSE)
      ->addWhere('contact_id', '=', $contactId)
      ->addWhere('group_id', '=', $groupId)
      ->execute();
    if ($existingRecord->count()) {
      $recordId = $existingRecord->first()['id'];
      $status = $existingRecord->first()['status'];
      if ($status != 'Removed') {
        $results = \Civi\Api4\GroupContact::update(FALSE)
          ->addWhere('id', '=', $recordId)
          ->addValue('status', 'Removed')
          ->execute();
      }
    }
  }

}
