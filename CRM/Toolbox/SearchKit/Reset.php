<?php

class CRM_Toolbox_SearchKit_Reset {
  /**
   * Resetowanie widoków, formularzy etc. dla SearchKit
   * Przydatne przy upgrade etc. dzięki czemu nie trzeba wł/wył rozszerzenie
   *
   * @param $file
   * @return array
   */
  public static function fromFile($file): array {
    $reverted = [];
    if(is_file($file)) {
      $definitions = include($file);

      foreach ($definitions as $definition) {
        try {
          switch ($definition['entity']) {
            case 'SavedSearch':
              $entity = \Civi\Api4\SavedSearch::revert(FALSE);
              break;
            case 'SearchDisplay':
              $entity = \Civi\Api4\SearchDisplay::revert(FALSE);
              break;
          }
          if(isset($entity)) {
            $reverted[] = $entity->addWhere('name', '=', $definition['params']['values']['name'])
              ->execute()
              ->single()['id'];
          }
        } catch (CRM_Core_Exception $e) {
          $reverted[] = [
            $e->getMessage(),
            $definition['params']['values']['name']
          ];
        }
      }
    }
    return $reverted;
  }
}
