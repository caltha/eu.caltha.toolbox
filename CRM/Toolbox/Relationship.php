<?php

trait CRM_Toolbox_Relationship {

  /**
   * Get all relationships between two contacts on given type of relationship
   * @param int $typeId
   * @param int $contactIdA
   * @param int $contactIdB
   * @param array $options
   *
   * @return array
   * @throws \CRM_Core_Exception
   * @throws \Civi\API\Exception\UnauthorizedException
   */
  public static function get(int $typeId, int $contactIdA, int $contactIdB, array $options = []): array {
    $relationships = \Civi\Api4\Relationship::get(FALSE)
      ->addWhere('relationship_type_id', '=', $typeId)
      ->addWhere('contact_id_a', '=', $contactIdA)
      ->addWhere('contact_id_b', '=', $contactIdB);
    foreach ($options as $key => $value) {
      $relationships->addWhere($key, '=', $value);
    }

    return $relationships->execute()->getArrayCopy();
  }

  /**
   * Get only active relationships between two contacts on given type of
   * relationship
   * @param int $typeId
   * @param int $contactIdA
   * @param int $contactIdB
   * @param array $options
   *
   * @return array
   * @throws \CRM_Core_Exception
   * @throws \Civi\API\Exception\UnauthorizedException
   */
  public static function getActive(int $typeId, int $contactIdA, int $contactIdB, array $options = []): array {
    return self::get($typeId, $contactIdA, $contactIdB, array_merge($options, ['is_active' => 1]));
  }

  /**
   * Get first active relationship between two contacts on given type of
   * relationship
   * @param int $typeId
   * @param int $contactIdA
   * @param int $contactIdB
   * @param array $options
   *
   * @return array
   * @throws \CRM_Core_Exception
   * @throws \Civi\API\Exception\UnauthorizedException
   */
  public static function getFirstActive(int $typeId, int $contactIdA, int $contactIdB, array $options = []): array {
    $relationships = self::getActive($typeId, $contactIdA, $contactIdB, $options);
    foreach ($relationships as $relationship) {
      return $relationship;
    }

    return [];
  }

  /**
   * @param int $typeId
   * @param int $contactIdA
   * @param int $contactIdB
   * @param array $options
   *
   * @return array
   * @throws \CRM_Core_Exception
   * @throws \Civi\API\Exception\UnauthorizedException
   */
  public static function create(int $typeId, int $contactIdA, int $contactIdB, array $options = []): array {
    $relationships = \Civi\Api4\Relationship::create(FALSE)
      ->addValue('relationship_type_id', $typeId)
      ->addValue('contact_id_a', $contactIdA)
      ->addValue('contact_id_b', $contactIdB);
    foreach ($options as $key => $value) {
      $relationships->addValue($key, $value);
    }

    return $relationships->execute()->getArrayCopy();
  }

  /**
   * @param int $typeId
   * @param int $contactIdA
   * @param int $contactIdB
   * @param array $options
   *
   * @return array
   * @throws \CRM_Core_Exception
   * @throws \Civi\API\Exception\UnauthorizedException
   */
  public static function createActiveFromNow(int $typeId, int $contactIdA, int $contactIdB, array $options = []): array {
    return self::create($typeId, $contactIdA, $contactIdB, array_merge($options, ['is_active' => 1, 'start_date' => date('Y-m-d')]));
  }
}
