<?php

use Civi\API\Exception\UnauthorizedException;

trait CRM_Toolbox_Options {

  /**
   * Get or create new option group (without cache).
   *
   * @param string $name
   * @param array $options supported fields:
   * - title
   * - description
   * - data_type
   * - is_reserved
   * - is_active
   * - is_locked
   *
   * @return int
   * @throws API_Exception
   * @throws UnauthorizedException
   */
  public static function setGroup(string $name, array $options = []): int {
    try {
      $optionGroup = \Civi\Api4\OptionGroup::get(false)
        ->addSelect('id')
        ->addWhere('name', '=', $name)
        ->execute()
        ->single();
    } catch (API_Exception $exception) {
      $options = array_replace(['title' => $name], $options);
      $optionGroupCreate = \Civi\Api4\OptionGroup::create(false)
        ->addValue('name', $name);
      foreach ($options as $key => $value) {
        $optionGroupCreate->addValue($key, $value);
      }
      $optionGroup = $optionGroupCreate->execute()->single();
    }

    return (int) $optionGroup['id'];
  }

  /**
   * Get or create new option value
   *
   * @param string $optionGroupName
   * @param string $name
   * @param array $options all fields from \Civi\Api4\OptionValue are supported, for example:
   * - label
   * - value
   * - description
   * - filter
   * - icon
   * - color
   * @return mixed 'value' is returned
   * @throws API_Exception
   * @throws UnauthorizedException
   */
  public static function setValue(string $optionGroupName, string $name, array $options = []) {
    try {
      $optionValue = \Civi\Api4\OptionValue::get(false)
        ->addSelect('value')
        ->addWhere('option_group_id.name', '=', $optionGroupName)
        ->addWhere('name', '=', $name)
        ->execute()
        ->single();
    } catch (API_Exception $exception) {
      $options = array_replace(['label' => $name], $options);
      $optionValueCreate = \Civi\Api4\OptionValue::create(false)
        ->addValue('option_group_id.name', $optionGroupName)
        ->addValue('name', $name);
      foreach ($options as $key => $value) {
        $optionValueCreate->addValue($key, $value);
      }
      $optionValue = $optionValueCreate->execute()->single();
    }

    return $optionValue['value'];
  }

  /**
   * Allow to erase custom option group with fields, ex: while testing
   *
   * @param $groupName
   * @return true
   */
  public static function eraseCustomGroup($groupName) {
    try {
        $result = \Civi\Api4\OptionGroup::get(FALSE)
            ->addWhere('name', '=', $groupName)
            ->execute()
            ->single();

        if ($result) {
            \Civi\Api4\OptionGroup::delete(FALSE)
                ->addWhere('id', '=', $result['id'])
                ->execute();
        }

    } catch (API_Exception $exception) {}

    return TRUE;
  }
}
