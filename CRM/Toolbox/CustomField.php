<?php

trait CRM_Toolbox_CustomField {

  private static $customFieldPrefix = 'custom_';

  /**
   * @param string $groupName
   * @param string $fieldName
   *
   * @return string
   * @throws \CiviCRM_API3_Exception
   */
  protected static function getCustomField($groupName, $fieldName) {
    $result = civicrm_api3('CustomField', 'get', [
      'sequential' => 1,
      'custom_group_id' => $groupName,
      'name' => $fieldName,
    ]);

    return self::$customFieldPrefix . $result['id'];
  }

  /**
   * Get custom field name for given group.
   * Field name in api format, custom_1
   *
   * @param string $key
   * @param string $groupName
   * @param string $fieldName
   *
   * @return string
   * @throws \CiviCRM_API3_Exception
   */
  public static function get($key, $groupName, $fieldName) {
    $cache = Civi::cache('long')->get($key);
    if (!isset($cache)) {
      $id = self::getCustomField($groupName, $fieldName);
      Civi::cache('long')->set($key, $id);
      return $id;
    }

    return $cache;
  }

  /**
   * Don't forget to pass in $options
   * - "label"
   * - "column_name"
   * - "weight"
   *
   * @param string $key
   * @param string $fieldName
   * @param string $groupName
   * @param array $options
   * @return string
   * @throws CiviCRM_API3_Exception
   */
  protected static function setSimpleText(string $key, string $fieldName, string $groupName, array $options = []): string {
    $options = array_replace(
      [
        "data_type" => "String",
        "html_type" => "Text",
        "is_searchable" => "1",
        "is_search_range" => "0",
        "is_active" => "1",
        "text_length" => "255",
        "note_columns" => "60",
        "note_rows" => "4",
        "is_view" => "0",
      ],
      $options
    );
    return self::set($key, $fieldName, $groupName, $options);
  }

  /**
   * Note with TextArea field type.
   *
   * Don't forget to pass in $options
   * - "label"
   * - "column_name"
   * - "weight"
   * - "html_type": [ TextArea | RichTextEditor ]
   *
   * @param string $key
   * @param string $fieldName
   * @param string $groupName
   * @param array $options
   * @return string
   * @throws CiviCRM_API3_Exception
   */
  protected static function setNote(string $key, string $fieldName, string $groupName, array $options = []): string {
    $options = array_replace(
      [
        "data_type" => "Memo",
        "html_type" => "TextArea",
        "is_searchable" => "1",
        "is_search_range" => "0",
        "is_active" => "1",
        "text_length" => "255",
        "note_columns" => "60",
        "note_rows" => "4",
        "is_view" => "0",
      ],
      $options
    );
    return self::set($key, $fieldName, $groupName, $options);
  }

  /**
   * Don't forget to pass in $options
   * - "label"
   * - "column_name"
   * - "weight"
   * - "option_group_id"
   *
   * @param string $key
   * @param string $fieldName
   * @param string $groupName
   * @param array $options
   * @return string
   * @throws CiviCRM_API3_Exception
   */
  protected static function setRadioText(string $key, string $fieldName, string $groupName, array $options = []): string {
    $options = array_replace(
      [
        "data_type" => "String",
        "html_type" => "Radio",
        "is_searchable" => "1",
        "is_search_range" => "0",
        "is_active" => "1",
        "text_length" => "255",
        "note_columns" => "60",
        "note_rows" => "4",
        "is_view" => "0",
      ],
      $options
    );
    return self::set($key, $fieldName, $groupName, $options);
  }

  /**
   * Don't forget to pass in $options
   * - "label"
   * - "column_name"
   * - "weight"
   * - "option_group_id"
   *
   * @param string $key
   * @param string $fieldName
   * @param string $groupName
   * @param array $options
   * @return string
   * @throws CiviCRM_API3_Exception
   */
  protected static function setSelectText(string $key, string $fieldName, string $groupName, array $options = []): string {
    $options = array_replace(
      [
        "data_type" => "String",
        "html_type" => "Select",
        "is_searchable" => "1",
        "is_search_range" => "0",
        "is_active" => "1",
        "text_length" => "255",
        "note_columns" => "60",
        "note_rows" => "4",
        "is_view" => "0",
      ],
      $options
    );
    return self::set($key, $fieldName, $groupName, $options);
  }

  /**
   * Yes (value 1) or Not (value 0), Built-in boolean radio.
   * Don't forget to pass in $options
   * - "label"
   * - "column_name"
   * - "weight"
   *
   * @param string $key
   * @param string $fieldName
   * @param string $groupName
   * @param array $options
   * @return string
   * @throws CiviCRM_API3_Exception
   */
  protected static function setYesNo(string $key, string $fieldName, string $groupName, array $options = []): string {
    $options = array_replace(
      [
        "data_type" => "Boolean",
        "html_type" => "Radio",
        "is_searchable" => "1",
        "is_search_range" => "0",
        "is_active" => "1",
        "text_length" => "255",
        "note_columns" => "60",
        "note_rows" => "4",
        "is_view" => "0",
      ],
      $options
    );
    return self::set($key, $fieldName, $groupName, $options);
  }

  /**
   * DateTime in format YYYY-MM-DD HH:mm
   * Don't forget to pass in $options
   * - "label"
   * - "column_name"
   * - "weight"
   *
   * @param string $key
   * @param string $fieldName
   * @param string $groupName
   * @param array $options
   * @return string
   * @throws CiviCRM_API3_Exception
   */
  protected static function setDateTime(string $key, string $fieldName, string $groupName, array $options = []): string {
    $options = array_replace(
      [
        "data_type" => "Date",
        "html_type" => "Select Date",
        "is_searchable" => "1",
        "is_search_range" => "0",
        "is_active" => "1",
        "text_length" => "255",
        "note_columns" => "60",
        "note_rows" => "4",
        "is_view" => "0",
        "date_format" => 'yy-mm-dd',
        "time_format" => '2' // HH:mm
      ],
      $options
    );
    return self::set($key, $fieldName, $groupName, $options);
  }

  /**
   * Don't forget to pass in $options
   * - "label"
   * - "column_name"
   * - "weight"
   *
   * @param string $key
   * @param string $fieldName
   * @param string $groupName
   * @param array $options
   * @return string
   * @throws CiviCRM_API3_Exception
   */
  protected static function setLink(string $key, string $fieldName, string $groupName, array $options = []): string {
    $options = array_replace(
      [
        "data_type" => "Link",
        "html_type" => "Link",
        "is_searchable" => "1",
        "is_search_range" => "0",
        "is_active" => "1",
        "text_length" => "255",
        "note_columns" => "60",
        "note_rows" => "4",
      ],
      $options
    );
    return self::set($key, $fieldName, $groupName, $options);
  }

  /**
   * Don't forget to pass in $options
   * - "label"
   * - "column_name"
   * - "weight"
   *
   * And optional option:
   * - "filter":
   *   - contacts with subtype: action=get&contact_sub_type=CONTACT_SUB_TYPE_NAME
   *   - contacts within group: action=lookup&group=GROUP_ID
   *
   * @param string $key
   * @param string $fieldName
   * @param string $groupName
   * @param array $options [ 'label' | 'column_name' | 'weight' | 'filter' ]
   * @return string
   * @throws CiviCRM_API3_Exception
   */
  protected static function setContactReference(string $key, string $fieldName, string $groupName, array $options = []): string {
    $options = array_replace(
      [
        "data_type" => "ContactReference",
        "html_type" => "Autocomplete-Select",
        "is_searchable" => "1",
        "is_search_range" => "0",
        "is_active" => "1",
        "text_length" => "255",
        "note_columns" => "60",
        "note_rows" => "4",
      ],
      $options
    );
    return self::set($key, $fieldName, $groupName, $options);
  }

  /**
   * Don't forget to pass in $options
   * - "fk_entity"
   * - "label"
   * - "column_name"
   * - "weight"
   *
   * And optional option:
   * - "filter":
   *   - info: field=value&another_field=val1,val2
   *   - list Students in "Volunteers" or "Supporters" groups: contact_sub_type=Student&groups:name=Volunteers,Supporters
   *
   * @param string $key
   * @param string $fieldName
   * @param string $groupName
   * @param array $options [ 'label' | 'column_name' | 'weight' | 'filter' ]
   * @return string
   * @throws CiviCRM_API3_Exception
   */
  protected static function setEntityReference(string $key, string $fieldName, string $groupName, array $options = []): string {
    $options = array_replace(
      [
        "data_type" => "EntityReference",
        "html_type" => "Autocomplete-Select",
        "is_searchable" => "1",
        "is_search_range" => "0",
        "is_active" => "1",
        "text_length" => "255",
        "note_columns" => "60",
        "note_rows" => "4",
      ],
      $options
    );
    return self::set($key, $fieldName, $groupName, $options);
  }

  /**
   * Don't forget to pass in $options
   * - "label"
   * - "column_name"
   * - "weight"
   *
   * @param string $key
   * @param string $fieldName
   * @param string $groupName
   * @param array $options [ 'label' | 'column_name' | 'weight' | 'filter' ]
   * @return string
   * @throws CiviCRM_API3_Exception
   */
  protected static function setAutocomplete(string $key, string $fieldName, string $groupName, array $options = []): string {
    $options = array_replace(
      [
        "data_type" => "String",
        "html_type" => "Autocomplete-Select",
        "is_searchable" => "1",
        "is_search_range" => "0",
        "is_active" => "1",
        "text_length" => "255",
        "note_columns" => "60",
        "note_rows" => "4",
        "serialize" => "0",
      ],
      $options
    );
    return self::set($key, $fieldName, $groupName, $options);
  }

  /**
   * Don't forget to pass in $options
   * - "label"
   * - "column_name"
   * - "weight"
   *
   * @param string $key
   * @param string $fieldName
   * @param string $groupName
   * @param array $options [ 'label' | 'column_name' | 'weight' | 'filter' ]
   * @return string
   * @throws CiviCRM_API3_Exception
   */
  protected static function setAutocompleteMulti(string $key, string $fieldName, string $groupName, array $options = []): string {
    $options = array_replace(
      [
        "serialize" => "1",
      ],
      $options
    );
    return self::setAutocomplete($key, $fieldName, $groupName, $options);
  }

  /**
   * Don't forget to pass in $options
   * - "label"
   * - "column_name"
   * - "weight"
   *
   * @param string $key
   * @param string $fieldName
   * @param string $groupName
   * @param array $options [ 'label' | 'column_name' | 'weight' | 'filter' ]
   * @return string
   * @throws CiviCRM_API3_Exception
   */
  protected static function setIntegerSingle(string $key, string $fieldName, string $groupName, array $options = []): string {
    $options = array_replace(
      [
        "data_type" => "Int",
        "html_type" => "Text",
        "is_searchable" => "1",
        "is_search_range" => "0",
        "is_active" => "1",
        "text_length" => "255",
      ],
      $options
    );
    return self::set($key, $fieldName, $groupName, $options);
  }

  /**
   * Don't forget to pass in $options
   * - "label"
   * - "column_name"
   * - "weight"
   *
   * @param string $key
   * @param string $fieldName
   * @param string $groupName
   * @param array $options [ 'label' | 'column_name' | 'weight' | 'filter' ]
   * @return string
   * @throws CiviCRM_API3_Exception
   */
  protected static function setNumberSingle(string $key, string $fieldName, string $groupName, array $options = []): string {
    $options = array_replace(
      [
        "data_type" => "Number",
        "html_type" => "Text",
        "is_searchable" => "1",
        "is_search_range" => "0",
        "is_active" => "1",
        "text_length" => "255",
      ],
      $options
    );
    return self::set($key, $fieldName, $groupName, $options);
  }

  /**
   * Don't forget to pass in $options
   * - "label"
   * - "column_name"
   * - "weight"
   *
   * @param string $key
   * @param string $fieldName
   * @param string $groupName
   * @param array $options [ 'label' | 'column_name' | 'weight' | 'filter' ]
   * @return string
   * @throws CiviCRM_API3_Exception
   */
  protected static function setMoneySingle(string $key, string $fieldName, string $groupName, array $options = []): string {
    $options = array_replace(
      [
        "data_type" => "Money",
        "html_type" => "Text",
        "is_searchable" => "1",
        "is_search_range" => "0",
        "is_active" => "1",
        "text_length" => "255",
      ],
      $options
    );
    return self::set($key, $fieldName, $groupName, $options);
  }

  /**
   * @param string $key
   * @param string $fieldName
   * @param string $groupName
   * @param array $options
   * @return string
   * @throws CiviCRM_API3_Exception
   */
  protected static function set(string $key, string $fieldName, string $groupName, array $options = []): string {
    $cache = Civi::cache('long')->get($key);
    if (!isset($cache)) {
      $id = self::setField($fieldName, $groupName, $options);
      Civi::cache('long')->set($key, $id);
      return $id;
    }

    return $cache;
  }

  /**
   * @param string $fieldName
   * @param string $groupName
   * @param array $options keys: name, label, data_type
   * @return string
   * @throws CiviCRM_API3_Exception
   */
  private static function setField(string $fieldName, string $groupName, array $options = []): string {
    $result = civicrm_api3('CustomField', 'get', [
      'sequential' => 1,
      'name' => $fieldName,
      'custom_group_id' => $groupName,
    ]);
    if ($result['count'] == 0) {
      $params = [
          'sequential' => 1,
          'name' => $fieldName,
          'custom_group_id' => $groupName,
        ] + $options;
      $result = civicrm_api3('CustomField', 'create', $params);
    }

    return self::$customFieldPrefix . $result['id'];
  }

  /**
   * @param array $fields Array from hooks where indexes of custom fields have format: custom_245_3363
   * @param string $customFieldId Long name of custom field in format: custom_245
   *
   * @return string
   */
  public static function findIndex(array $fields, string $customFieldId): string {
    foreach ($fields as $index => $field) {
      $pattern = '/^' . $customFieldId . '\_/';
      if (preg_match($pattern, $index, $matches)) {
        return $index;
      }
    }

    return $customFieldId;
  }

  public static function installCustomDataFields(string $className){
    $f = new ReflectionClass($className);
    $methodsNotAlowed = ['setGroup', 'setValue', 'eraseCustomGroup', 'optionGroupId'];
    foreach ($f->getMethods() as $m) {
      if ($m->class == $className && !in_array($m->name, $methodsNotAlowed)) {
        forward_static_call_array([$className, $m->name], []);
      }
    }
  }
}
