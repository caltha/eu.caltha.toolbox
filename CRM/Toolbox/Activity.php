<?php

trait CRM_Toolbox_Activity {

  /**
   * Url to activity
   *
   * @param int $activityTypeId
   * @param int $id Id aktywności
   * @param int $contactId
   * @return string
   */
  public static function url(int $activityTypeId, int $id, int $contactId): string {
    return CRM_Utils_System::url(
      'civicrm/activity',
      [
        'atype' => $activityTypeId,
        'action' => 'view',
        'reset' => 1,
        'id' => $id,
        'cid' => $contactId,
      ],
      TRUE
    );
  }

  /**
   * @param int $sourceContactId
   * @param int $targetContactId
   * @param string $subject
   * @param string $html
   * @return array
   * @throws CiviCRM_API3_Exception
   */
  public static function saveEmail(int $sourceContactId, int $targetContactId, string $subject, string $html): array {
    $activityTypeId = CRM_Core_PseudoConstant::getKey('CRM_Activity_BAO_Activity', 'activity_type_id', 'Email');
    $params = [
      'sequential' => 1,
      'source_contact_id' => $sourceContactId,
      'activity_type_id' => $activityTypeId,
      'activity_date_time' => date('YmdHis'),
      'status_id' => 'Completed',
      'subject' => $subject,
      'details' => $html,
      'api.ActivityContact.create' => [
        0 => [
          'activity_id' => '$value.id',
          'contact_id' => $targetContactId,
          'record_type_id' => 3,
        ],
      ],
    ];

    return civicrm_api3('Activity', 'create', $params);
  }

}
