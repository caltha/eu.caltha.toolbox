<?php

class CRM_Toolbox_Format {

  /**
   * Format multibyte string
   *
   * @param $string
   *
   * @return string
   */
  private static function mbUcfirst($string) {
    $firstChar = mb_strtoupper(mb_substr($string, 0, 1));
    return $firstChar . mb_substr($string, 1);
  }

  /**
   * Format each word in string separate by space or dash
   *
   * @param $string
   *
   * @return string
   */
  public static function mbUcwords($string) {
    $delimiters = ['-', ' '];
    $string = mb_strtolower($string);
    $missing_delimiters = 0;
    foreach ($delimiters as $delimiter) {
      if (strpos($string, $delimiter) !== FALSE) {
        $tab = explode($delimiter, $string);
        foreach ($tab as $k => $v) {
          if (mb_strlen($v) > 1) {
            $v1 = self::mbUcfirst($v);
            $tab[$k] = self::roman($v1);
          }
        }
        $string = implode($delimiter, $tab);
      }
      else {
        $missing_delimiters++;
      }
    }
    if ($missing_delimiters == count($delimiters)) {
      $string = self::mbUcfirst($string);
      $string = self::roman($string);
    }

    return $string;
  }


  /**
   * Upper string if is roman number.
   *
   * @param string $string
   *
   * @return string
   */
  private static function roman($string) {
    $re = '/^([ivxlcdm]{2,})+$/i';
    if (preg_match_all($re, $string, $matches, PREG_SET_ORDER, 0)) {
      return strtoupper($string);
    }

    return $string;
  }

}
