<?php

trait CRM_Toolbox_Case {

  /**
   * Url to case
   *
   * @param int $id case id
   * @param int $contactId contact id
   * @return string
   */
  public static function url(int $id, int $contactId): string {
    return CRM_Utils_System::url(
      'civicrm/contact/view/case',
      [
        'reset' => 1,
        'id' => $id,
        'cid' => $contactId,
        'context' => 'case',
        'action' => 'view',
      ],
      TRUE
    );
  }

}
