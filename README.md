# Caltha Toolbox for CiviCRM

## Example

```php
class CRM_Dontus_Model_Group {
  use CRM_Toolbox_Group;

  const CUSTOM_GROUP = 'Custom group name';

  /**
   * @return int|mixed
   * @throws \CiviCRM_API3_Exception
   */
  function customGroup() {
    return self::set(__METHOD__, self::CUSTOM_GROUP);
  }
}
```

## Example of defining payment method with financial account

```php
class CRM_Bluepayment_Model_PaymentMethod {
  use CRM_Toolbox_PaymentMethod;

  private const BLUE_MEDIA = 'Blue Media';

  /**
   * @return int
   * @throws CiviCRM_API3_Exception
   */
  public static function blueMedia(): int {
    return (int) self::set(__METHOD__, self::BLUE_MEDIA, [
       'financial_account' => [
        'name' => 'BlueMedia Financial Account',
        'type' => 'Asset',
        'accounting_code' => 'BMTRX',
        'is_active' => 1,
        'is_tax' => FALSE,
        'is_deductible' => FALSE,
        'is_header_account' => FALSE,
      ]
    ]);
  }

}
```

## Example for activity type with translated label

```php
<?php

use CRM_HistoEnto_ExtensionUtil as E;

class CRM_HistoEnto_Model_ActivityType {
  use CRM_Toolbox_ActivityType;

  /**
   * @return int
   * @throws \CiviCRM_API3_Exception
   */
  public static function tagAdded(): int {
    return self::set(__METHOD__, 'Tag added', ['label' => E::ts('Tag added')]);
  }

}
```

## Testing

All test will be prepared in phpunit.
Testing available with CiviCRM that was created via [buildkit](https://docs.civicrm.org/dev/en/latest/tools/buildkit/) and [civibuild](https://docs.civicrm.org/dev/en/latest/tools/civibuild/)

Also You need to make sure in the civicrm directory ( sites/all/modules/civicrm ) you have downloaded the tests/ directory: [https://github.com/civicrm/](https://github.com/civicrm/civicrm-core/tree/master/tests)

Run tests
```
buildkit@civicrm:...ext/eu.caltha.toolbox$ phpunit5 ./tests/phpunit/CRM/Toolbox/ContactUnitTest.php
```
